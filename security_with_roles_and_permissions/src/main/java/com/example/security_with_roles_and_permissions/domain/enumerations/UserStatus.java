package com.example.security_with_roles_and_permissions.domain.enumerations;

public enum UserStatus {
    ACTIVE,
    EXPIRED,
    BLOCKED,
    FROZEN
}
