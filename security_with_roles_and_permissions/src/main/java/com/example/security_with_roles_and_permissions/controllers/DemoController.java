package com.example.security_with_roles_and_permissions.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DemoController {

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    @GetMapping("/user")
    public String user() {
        return "user";
    }

    @GetMapping("/user-read")
    public String userGet() {
        return "user read";
    }

    @PostMapping("/user-create")
    public String userPost() {
        return "user create";
    }

    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }

    @GetMapping("/admin-read")
    public String adminGet() {
        return "admin read";
    }

    @PostMapping("/admin-create")
    public String adminPost() {
        return "admin create";
    }
}
