package com.example.security_with_roles_and_permissions.security;

import com.example.security_with_roles_and_permissions.domain.entities.Authority;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public class GrantedAuthorityImpl implements GrantedAuthority {

    private final Authority authority;

    @Override
    public String getAuthority() {
        return authority.getAuthorityType().name();
    }
}
