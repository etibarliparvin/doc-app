package com.example.security_with_roles_and_permissions;

import com.example.security_with_roles_and_permissions.domain.entities.Authority;
import com.example.security_with_roles_and_permissions.domain.entities.Role;
import com.example.security_with_roles_and_permissions.domain.entities.User;
import com.example.security_with_roles_and_permissions.domain.enumerations.AuthorityType;
import com.example.security_with_roles_and_permissions.domain.enumerations.RoleType;
import com.example.security_with_roles_and_permissions.repositories.UserRepository;
import com.example.security_with_roles_and_permissions.security.UserDetailsImpl;
import com.example.security_with_roles_and_permissions.security.jwt.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
public class SecurityWithRolesAndPermissionsApplication implements CommandLineRunner {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final TokenProvider tokenProvider;

    public static void main(String[] args) {
        SpringApplication.run(SecurityWithRolesAndPermissionsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Authority authorityRead = new Authority();
//        authorityRead.setAuthorityType(AuthorityType.READ);
//        Set<Authority> authorities = Set.of(authorityRead);
//
//        Role roleUser = new Role();
//        roleUser.setType(RoleType.USER);
//        roleUser.setAuthorities(authorities);
//        Set<Role> roles = Set.of(roleUser);
//
//        User parvin = new User();
//        parvin.setEmail("parvin@gmail.com");
//        parvin.setPassword(passwordEncoder.encode("password"));
//        parvin.setRoles(roles);
//        repository.save(parvin);
        Optional<User> parvin = repository.getUsersByUsername("parvin@gmail.com");
        System.out.println(parvin);
        String token = tokenProvider.createToken(new UserDetailsImpl(parvin.get()));
        System.out.println(token);
        System.out.println(tokenProvider.extractAllClaims(token));
    }
}
