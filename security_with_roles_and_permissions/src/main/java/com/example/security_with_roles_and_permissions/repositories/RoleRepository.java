package com.example.security_with_roles_and_permissions.repositories;

import com.example.security_with_roles_and_permissions.domain.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
