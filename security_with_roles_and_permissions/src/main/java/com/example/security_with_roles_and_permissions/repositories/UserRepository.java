package com.example.security_with_roles_and_permissions.repositories;

import com.example.security_with_roles_and_permissions.domain.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u join fetch u.roles r join fetch r.authorities where u.email = :email")
    Optional<User> getUsersByUsername(@Param(value = "email") String email);
}
