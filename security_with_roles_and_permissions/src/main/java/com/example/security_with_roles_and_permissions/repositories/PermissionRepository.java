package com.example.security_with_roles_and_permissions.repositories;

import com.example.security_with_roles_and_permissions.domain.entities.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<Authority, Long> {
}
