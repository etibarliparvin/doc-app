package com.example.alibou;

import com.example.alibou.domain.entities.Role;
import com.example.alibou.domain.entities.User;
import com.example.alibou.domain.enumerations.RoleType;
import com.example.alibou.security.UserDetailsImpl;
import com.example.alibou.security.jwt.TokenProvider;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

@SpringBootApplication
@RequiredArgsConstructor
public class AlibouApplication implements CommandLineRunner {

    private final UserRepository repository;
    private final TokenProvider tokenProvider;
    private final PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(AlibouApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Role userRole = new Role();
//        userRole.setRoleType(RoleType.ROLE_USER);
//        Set<Role> roles = Set.of(userRole);
//
//        User narmin = new User();
//        narmin.setEmail("email");
//        narmin.setPassword(passwordEncoder.encode("password"));
//        narmin.setRoles(roles);
//
//        User save = repository.save(narmin);
//        System.out.println(save);

        Optional<User> narmin = repository.findUserByEmail("email");
        System.out.println(narmin);
        String token = tokenProvider.createToken(new UserDetailsImpl(narmin.get()));
        System.out.println(token);
    }
}
