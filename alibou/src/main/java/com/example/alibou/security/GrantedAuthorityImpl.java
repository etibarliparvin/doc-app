package com.example.alibou.security;

import com.example.alibou.domain.entities.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public class GrantedAuthorityImpl implements GrantedAuthority {

    private final Role role;

    @Override
    public String getAuthority() {
        return role.getRoleType().name();
    }
}
