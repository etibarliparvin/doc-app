package com.example.alibou.domain.enumerations;

public enum UserStatus {
    ACTIVE,
    BLOCKED,
    FROZEN,
    DELETED
}
