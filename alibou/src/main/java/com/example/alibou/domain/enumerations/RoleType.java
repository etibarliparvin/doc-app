package com.example.alibou.domain.enumerations;

public enum RoleType {
    ROLE_USER,
    ROLE_ADMIN
}
