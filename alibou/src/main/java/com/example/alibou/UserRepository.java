package com.example.alibou;

import com.example.alibou.domain.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u join fetch u.roles where u.email= :email")
    Optional<User> findUserByEmail(@Param(value = "email") String email);
}
