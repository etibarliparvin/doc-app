package com.doc.auth.domain.enumerations;

public enum UserStatus {
    ACTIVE,
    EXPIRED,
    BLOCKED,
    FROZEN
}
