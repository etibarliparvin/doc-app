package com.doc.auth.domain.enumerations;

public enum RoleType {
    USER,
    ADMIN
}
