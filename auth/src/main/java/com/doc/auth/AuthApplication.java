package com.doc.auth;

import com.doc.auth.domain.entites.Role;
import com.doc.auth.domain.entites.User;
import com.doc.auth.domain.enumerations.RoleType;
import com.doc.auth.repository.UserRepository;
import com.doc.auth.security.UserDetailImpl;
import com.doc.auth.security.jwt.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;

@EnableJpaAuditing
@SpringBootApplication
@RequiredArgsConstructor
public class AuthApplication implements CommandLineRunner {

    private final UserRepository repository;
    private final TokenProvider tokenProvider;

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Role roleUser = new Role();
//        roleUser.setType(RoleType.USER);
//        Set<Role> roles = Set.of(roleUser);
//
//        User parvin = new User();
//        parvin.setUsername("parvin");
//        parvin.setPassword(new BCryptPasswordEncoder().encode("password"));
//        parvin.setAuthorities(roles);
//
//        repository.save(parvin);
        Optional<User> parvin = repository.getUsersByUsername("parvin");
        User user = parvin.get();
        System.out.println(user);
        String token = tokenProvider.createToken(new UserDetailImpl(user));
        System.out.println(token);
    }
}
