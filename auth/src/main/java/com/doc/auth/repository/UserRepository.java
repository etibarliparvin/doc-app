package com.doc.auth.repository;

import com.doc.auth.domain.entites.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u join fetch u.authorities where u.username = :username")
    Optional<User> getUsersByUsername(@Param(value = "username") String username);
}
