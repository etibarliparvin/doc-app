package com.doc.auth.security;

import com.doc.auth.domain.entites.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public class GrantedAuthorityImpl implements GrantedAuthority {

    private final Role role;

    @Override
    public String getAuthority() {
        return role.getType().name();
    }
}
